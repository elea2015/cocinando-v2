<?php /* Template name: homepage */ get_header(); ?>

<div class="hero">
    <h1 class="text-center">¿Qué quieres comer hoy?</h1>
    <form class="w-full max-w-lg" method="get" action="<?php echo home_url(); ?>">
        <div class="flex items-center bg-white">
            <input class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none" type="text" placeholder="Jane Doe" aria-label="Full name">
            <button class="flex-shrink-0 bg-orange-500 hover:bg-orange-700 text-sm border-orange-500 border-4 text-white py-3 px-3" type="button">Buscar</button>
        </div>
    </form>
</div>

<section class="homePost">
    <div class="container">
        <div class="grid">
            <?php
            $args_re = array(
                'post_type' => array('post'),
                'post_status' => array('published'),
                'posts_per_page' => 10,
                'order' => 'DESC',
            );

            $re = new WP_Query( $args_re );

            if ( $re->have_posts() ) : while ( $re->have_posts() ) : $re->the_post();
                    $link = get_the_permalink();
                    $title = get_the_title();
                    $image = get_the_post_thumbnail_url('recetas');
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="max-w-sm rounded overflow-hidden shadow-lg mb-6">
                    <a href="<?php echo $link ?>"><img class="w-full" src="<?php if( $image) {echo $image;}  else {echo get_template_directory_uri(); } ?>/img/blog.png" alt=""></a>
                    <div class="px-6 py-4">
                        <div class="font-bold text-xl mb-2"><a href="<?php echo $link ?>"><?php echo $title ?></a></div>
                    </div>
                    <div class="px-6 py-4">
                        <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">#photography</span>
                        <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">#travel</span>
                        <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700">#winter</span>
                    </div>
                </div>
            </article>
            <?php
            endwhile;
            endif;
            wp_reset_postdata();?>
         </div>   
    </div>
</section>

<?php get_footer(); ?>