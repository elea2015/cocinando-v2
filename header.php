<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<!-- Font -->
		<script src="https://kit.fontawesome.com/b186a1225f.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600&display=swap" rel="stylesheet">

		<!-- Css framework -->
		<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
		<!-- wp head -->
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>
		
		<nav class="flex items-center justify-between flex-wrap bg-gray-100 p-6">
			<div class="block lg:hidden">
				<button class="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
				<svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
				</button>
			</div>
			<div class="lg:flex">
				<div class="text-sm w-64">
					<a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-orange-500 hover:text-orange-900 mr-4">
						Docs
					</a>
					<a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-orange-500 hover:text-orange-900 mr-4">
						Examples
					</a>
					<a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-orange-500 hover:text-orange-900">
						Blog
					</a>
				</div>
			</div>
			<div class="text-orange-500">
				<!-- <svg class="fill-current h-8 w-8 mr-2" width="54" height="54" viewBox="0 0 54 54" xmlns="http://www.w3.org/2000/svg"><path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z"/></svg> -->
				<span class="font-semibold text-xl tracking-tight">Cocinan.do</span>
			</div>
			<div class="lg:flex">
				<div class="text-sm w-64 text-right">
					<a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-orange-500 hover:text-orange-900 mr-4">
						Docs
					</a>
					<a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-orange-500 hover:text-orange-900 mr-4">
						Examples
					</a>
					<a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-orange-500 hover:text-orange-900">
						Blog
					</a>
				</div>
			</div>
		</nav>
